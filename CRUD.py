
'''
usuarios = {
    'email': {
        'nombre': ''
        'edad': 00
        'password': ''
    }
}
'''


def crear_usuario(usuarios: dict):
    # Solicitar datos al usuario
    email = input('Email: ')
    nombre = input('Nombre: ').capitalize()
    edad = int(input('Edad: '))
    password = input('Password: ')
    # Añadir datos al diccionario
    usuarios[email] = {
        'nombre': nombre,
        'edad': edad,
        'password': password
    }
    print("\nUsuario creado con éxito\n")


def actualizar_usuario(usuarios: dict):
    # Solicitar email del usuario a actualizar
    email = input('Ingrese el email del usuario a actualizar: ')
    # Validar si el usuario existe en el diccionario
    if email in usuarios:
        password = input('Por favor ingrese la contraseña: ')
        #Validar la contraseña
        if password == usuarios[email]['password']:
            # Solicitar datos y actualizar diccionario
            usuarios[email] = {
                'nombre': input('Nombre: ').capitalize(),
                'edad': int(input('Edad: ')),
                'password': input('Password: ')
            }
            print(f'\nUsuario {email} actualizado con éxito\n')
        else:
            print('Contraseña incorrecta')
    else:
        print(f"El usuario con email {email} no existe en la base de datos")


def eliminar_usuario(usuarios: dict):
    # Solicitar email del usuario a actualizar
    email = input('Ingrese el email del usuario a actualizar: ')
    # Validar si el usuario existe en el diccionario
    if email in usuarios:
        # Eliminar usuario
        usuarios.pop(email)
        print(f'\nUsuario {email} eliminado con éxito\n')
    else:
        print(f"El usuario con email {email} no existe en la base de datos")

def consultar_todos_los_usuarios(usuarios: dict):
    for email, info in usuarios.items():
        nombre = info['nombre']
        edad = info['edad']
        password = info['password']
        print(f'----------------{email}------------------')
        print(f'Nombre: {nombre}')
        print(f'Edad: {edad}')
        print(f'Password: {password}')
        print('-------------------------------------------------')


def menu():
    # Diccionario contenedor de usuarios
    usuarios: dict = {
        'andres@gmail.com': {
            'nombre': 'Andres',
            'edad': 20,
            'password': '12345'
        },
        'juan@gmail.com': {
            'nombre': 'Juan',
            'edad': 25,
            'password': '12345678'
        }
    }

    mensaje_menu = "--------------CRUD USUARIOS-----------------\n"
    mensaje_menu += "1) Crear usuario\n"
    mensaje_menu += "2) Actualizar usuario\n"
    mensaje_menu += "3) Eliminar usuario\n"
    mensaje_menu += "4) Limpiar base de datos\n"
    mensaje_menu += "5) Consultar todos los usuarios\n"
    mensaje_menu += "6) Salir\n"
    mensaje_menu += ">>> "

    opcion = ''
    while opcion != 6:
        try:
            # Solicitar la opción al usuario
            opcion = int(input(mensaje_menu))
            # Condicionales
            if opcion == 1:
                crear_usuario(usuarios)
            elif opcion == 2:
                actualizar_usuario(usuarios)
            elif opcion == 3:
                eliminar_usuario(usuarios)
            elif opcion == 4:
                usuarios.clear()
                print('\nTodos los datos han sido eliminados\n')
            elif opcion == 5:
                consultar_todos_los_usuarios(usuarios)
            elif opcion != 6:
                print('\nIngrese una opción válida\n')
        except:
            print('\nDato inválido\n')


menu()
