'''
item-> Hace referencia a clave-valor
'''
estudiante_1: dict = {
    'nombre': 'Ana María',
    'apellido': 'Castillo',
    'edad': 24,
    'activo': False
}
#print(estudiante_1)
#Actualizar información
estudiante_1['apellido'] = 'Narvaez'
estudiante_1['activo'] = True
#print(estudiante_1)
#Añadir información
estudiante_1['promedio_notas'] = 4.5
#print(estudiante_1)

estudiante_2 = {
    'nombre': 'Ana María',
    'apellido': 'Castillo',
    'edad': 24,
    'activo': False,
    'promedio_notas': 0
}


estudiantes={
    '1234': estudiante_1,
    '4321': estudiante_2,
    'promedio_general': 0
}
print('--------------OBTENER VALORES DEL ESTUDIANTE 1------------')
info = estudiantes['1234']
print(info)
#print(estudiantes)

print('--------------OBTENIENDO LAS CLAVES------------')

#Iterar diccionario obteniendo las claves
for x in estudiantes:
    print(x)
    #Acceder a los valores por medio de la llave
    #y = estudiantes[x]
    #print(y)

print('--------------OBTENIENDO LOS VALORES------------')

#Iterar diccionario obteniendo los valores
for x in estudiantes.values():
    print(x)


print('--------------OBTENIENDO CLAVE - VALOR------------')

#Iterar diccionario obteniendo clave-valor
for clave,valor in estudiantes.items():
    print(clave)
    print(valor)