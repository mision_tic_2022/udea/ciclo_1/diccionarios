

estudiantes: dict = {
    '1234': {
        'nombre': 'Ana María',
        'apellido': 'Castillo',
        'edad': 24,
        'activo': True,
        'notas': {
            'nota_1': 4.5,
            'nota_2': 3.1,
            'nota_3': 4.8,
            'nota_4': 5
        }
    },
    '4321': {
        'nombre': 'Juan',
        'apellido': 'Quintero',
        'edad': 30,
        'activo': True,
        'notas': {
            'nota_1': 4.1,
            'nota_2': 3.5,
            'nota_3': 4.2,
            'nota_4': 4.2
        }
    },
    '987': {
        'nombre': 'Jesus',
        'apellido': 'Correa',
        'edad': 24,
        'activo': True,
        'notas': {
            'nota_1': 4.4,
            'nota_2': 3.1,
            'nota_3': 4.8,
            'nota_4': 4.0
        }
    },
    '7890': {
        'nombre': 'María',
        'apellido': 'Quintero',
        'edad': 20,
        'activo': True,
        'notas': {
            'nota_1': 4.8,
            'nota_2': 3.8,
            'nota_3': 4.5,
            'nota_4': 4.9
        }
    }
}

'''
1) Encapsule el código anterior en una función que reciba como parámetro
    el diccionario 'estudiantes' ✔️
2) La función desarrollada en el punto 1 debe retornar un diccionario
    con el promedio de notas de cada estudiante. ✔️
        NOTA: 
            El diccionario a retornar debe tener la siguiente estructura:
            {
                '1234': 2.2
            }
            donde '1234' es la cédula del estudiante y 2.2 es el promedio de la nota
'''


def calcular_promedio(dict_estudiantes: dict):
    #Diccionario que representa los promedios de los estudiantes
    dict_promedio_estudiantes = dict()
    # Itera el diccionario principal 'estudiantes'
    for cedula, info in dict_estudiantes.items():
        # Obtener las notas del estudiante
        notas = info['notas']
        # Variable que representa el promedio de notas por estudiante
        promedio_estudiante = 0
        # Iterar diccionario 'notas'
        for n in notas.values():
            # Suma las notas del estudiante
            promedio_estudiante += n
        # Obtener el tamaño del diccionario
        # len -> obtener el tamaño (número de items) del diccionario
        cant_notas = len(notas)
        # Calcular el promedio de notas
        promedio_estudiante /= cant_notas
        #Añadir el promedio al diccionario 'dict_promedio_estudiantes'
        dict_promedio_estudiantes[cedula] = promedio_estudiante

    #Retornar diccionario con los promedios de los estudiantes
    return dict_promedio_estudiantes

#Llamar la función y enviarle como parámetro el diccionario 'estudiantes'
respuesta = calcular_promedio(estudiantes)
print(respuesta)