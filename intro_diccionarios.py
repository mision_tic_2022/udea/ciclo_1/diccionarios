
diccionario_vacio = {}
diccionario_vacio = dict()

miDiccionario = {
    'llave_1': 'Este es el valor de la llave_1',
    'llave_2': 50,
    'llave_3': 3.1416,
    'llave_4': True,
    'llave_5': [1,2,3,4,5]
}
#print(miDiccionario)

#Acceder a un valor del diccionario
x = miDiccionario['llave_1']
print(x)

miDiccionario['llave_1'] = "Hola mundo"
print(miDiccionario)

miDiccionario['llave_6'] = "Andrés"
print(miDiccionario)